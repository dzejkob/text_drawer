# -*- coding: utf-8 -*-

"""
Renders semi-graphics images in ANSI mode.
"""

import math

from text_drawer.core import (
    BaseTextDrawer, BaseColorPalette, UNICODE_CHAR_CODES
)


# ANSI_BG_VAL = '3'
# ANSI_FG_VAL = '4'
# ANSI_RESET_DIRECTIVE = '\\033[30m'  # resets bg color to black


class BlackWhiteColorPalette(BaseColorPalette):
    """
    Black and white color palette.
    """
    COLOR_MAP = (
        ('black', (0, 0, 0), 0),
        ('white', (255, 255, 255), 7),
    )

    def __init__(self, color_support=False):
        """
        Support for color is turned off by default, as rendering should
        relay on characters only.
        """
        super(BlackWhiteColorPalette, self).__init__(
            color_support=color_support
        )

    def render_text_pixel(self, upper_pixel, lower_pixel):
        """
        Required interface needed by the base class.
        """
        if self.color_support:
            raise NotImplementedError(
                'Color support is forbidden for this renderer. '
                'It have to be implemented differently.'
            )

        # upper pixel:
        index = self.closest_to_color(upper_pixel)
        upper_param = self.COLOR_MAP[index][2]
        # lower pixel:
        index = self.closest_to_color(lower_pixel)
        lower_param = self.COLOR_MAP[index][2]
        if upper_param == 7 and lower_param == 7:
            # both pixels white
            char = UNICODE_CHAR_CODES['full_block']
        elif upper_param == 7:
            # upper pixel white
            char = UNICODE_CHAR_CODES['upper_half_block']
        elif lower_param == 7:
            char = UNICODE_CHAR_CODES['lower_half_block']
        else:
            char = ' '
        return char


class BaseAnsiColorPalette(BaseColorPalette):
    """
    Base for ANSI color palettes.
    """

    def render_text_pixel(self, upper_pixel, lower_pixel, prev_rendered=None):
        """
        Required interface needed by the base class.
        """
        if not self.color_support:
            raise NotImplementedError(
                'Color support is required for this renderer. '
                'No color support have to be implemented differently.'
            )

        # upper pixel:
        index = self.closest_to_color(upper_pixel)
        upper_param = self.COLOR_MAP[index][2]
        # lower pixel:
        index = self.closest_to_color(lower_pixel)
        lower_param = self.COLOR_MAP[index][2]

        # we have to make fg/bg coloring even if full block, because
        # full_block appears to not cover all the space
        coloring = (
            u'\033[' + str(upper_param + 10) + u'm' +
            u'\033[' + str(lower_param) + u'm'
        )
        if upper_param == lower_param:
            # both pixels are the same color
            character = UNICODE_CHAR_CODES['full_block']
        else:
            # pixels differ
            character = UNICODE_CHAR_CODES['lower_half_block']

        rendered = u'{0}{1}'.format(coloring, character)
        if prev_rendered == rendered:
            rendered = character  # omit coloring

        return rendered


class Ansi8ColorPalette(BaseAnsiColorPalette):
    """
    ANSI 8 color palette.
    """
    COLOR_MAP = (
        ('black', (0, 0, 0), 30),
        ('red', (170, 0, 0), 31),
        ('green', (0, 170, 0), 32),
        ('brown', (170, 160, 0), 33),
        ('blue', (0, 0, 170), 34),
        ('magenta', (170, 0, 170), 35),
        ('cyan', (0, 170, 170), 36),
        ('gray', (170, 170, 170), 37),
    )


class Ansi16ColorPalette(BaseAnsiColorPalette):
    """
    ANSI 16 color palette.
    """
    COLOR_MAP = (
        ('black', (0, 0, 0), 30),
        ('red', (170, 0, 0), 31),
        ('green', (0, 170, 0), 32),
        ('brown', (170, 160, 0), 33),
        ('blue', (0, 0, 170), 34),
        ('magenta', (170, 0, 170), 35),
        ('cyan', (0, 170, 170), 36),
        ('gray', (170, 170, 170), 37),
        ('dark-gray', (85, 85, 85), 90),
        ('light-red', (255, 85, 85), 91),
        ('light-green', (85, 255, 85), 92),
        ('yellow', (255, 255, 85), 93),
        ('light-blue', (85, 85, 255), 94),
        ('light-magenta', (255, 85, 255), 95),
        ('light-cyan', (85, 255, 255), 96),
        ('white', (255, 255, 255), 97),
    )


class AnsiTextDrawer(BaseTextDrawer):
    """
    Renders color semi-graphics pictures from the given image.
    """
    TEXT_ROW_END_DIRECTIVE = u'\033[40m\n'
    PALETTE_CLASS = Ansi16ColorPalette

    def get_text_pixels(self, image, palette=None):
        """
        Builds TextPixel array from the image using ANSI escape codes

        :param image [Image]: PIL Image instance
        :param palette [Object]: custom ColorPalette instance
            to override default
        :returns [array(array)]: 2 dimensional array of TextPixels
        """
        width, height = image.size
        palette = palette or self.palette
        pixel_array = image.load()
        text_pixels = []

        for i in range(int(math.ceil(height / 2.0))):
            # for every two image pixel rows build one text pixel row:
            text_row = []
            rendered_pixel = None
            for x in range(width):
                # for every pixel in a row:
                upper_color = pixel_array[x, 2*i]
                try:
                    lower_color = pixel_array[x, 2*i + 1]
                except IndexError:
                    # if the image height is odd number then we will fall
                    # outside of the image in the last row
                    lower_color = (0, 0, 0)
                if not self.COMPRESS_ENABLED:
                    rendered_pixel = None
                    # reset previously rendered pixel to turn off compression
                rendered_pixel = palette.render_text_pixel(
                    upper_color, lower_color, rendered_pixel
                )
                text_row.append(rendered_pixel)
            text_pixels.append(text_row)

        return text_pixels
