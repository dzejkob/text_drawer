# -*- coding: utf-8 -*-

"""
EARLY VERSION - TO BE REFACTORED AND OPTIMIZED
"""

from PIL import Image


UNICODE_CHAR_CODES = {
    'upper_half_block': u'\u2580',
    'lower_half_block': u'\u2584',
    'full_block': u'\u2588'
}


class BaseColorPalette(object):
    """
    Base class for ColorTable used to map picture RGB colors
    to the colors from defined palette.
    """
    COLOR_MAP = None
    # Set in derived class. Should contain list of:
    # (color_label, rgb_values, result_value)

    def __init__(self, color_support=True):
        """
        Extracts palette_colors from the COLOR_MAP.

        :param color_support [bool]: should color support be enabled, if so
            then color directives will be added to the rendered output
        """
        self.color_support = color_support
        self.palette_colors = [entry[1] for entry in self.COLOR_MAP]

    def closest_to_color(self, color):
        """
        Returns index of a closest color in the COLOR_MAP

        :param color: RGB(A) values of a color
        :return: index of a COLOR_MAP item that this color is closest to
        """
        differences = []
        for palette_rgb in self.palette_colors:
            # calculating overall difference
            difference = (
                abs(palette_rgb[0] - color[0]) +
                abs(palette_rgb[1] - color[1]) +
                abs(palette_rgb[2] - color[2])
            )
            differences.append(difference)
        return differences.index(min(differences))

    def render_text_pixel(self, upper_pixel, lower_pixel, prev_rendered=None):
        """
        Prepares string responsible for displaying two
        vertically placed pixels.

        :param upper_pixel [list(3)]: RGB color values of an upper pixel
        :param lower_pixel [list(3)]: RGB color values of an lower pixel
        :param prev_rendered [str]: previously rendered text pixel
            (to allow data compression by not duplicate coloring)
        :return [str]: rendered pixel
        """
        raise NotImplementedError(
            'get_color_directive method should be implemented '
            'in the class derived from BaseColorPalette.'
        )


class BaseTextDrawer(object):
    """
    Base class for rendering semi-graphics strings.
    """
    # the following has to be set in derived class:
    TEXT_ROW_END_DIRECTIVE = None
    PALETTE_CLASS = None  # Color helper class derived from BaseColorPalette
    COMPRESS_ENABLED = True  # Rendered output compression - impl. in derived

    def __init__(self, max_dimensions=(80, 160)):
        """
        :param max_dimensions [list(2): max dimensions of the image,
            if it's larger, then ValueError will be thrown.
        """
        self.max_width = max_dimensions[0]
        self.max_height = max_dimensions[1]
        self.palette = self.PALETTE_CLASS()

    def image_is_valid(self, image):
        """
        Checks if the image is valid and satisfies the size restrictions.

        :param image [Image]: PIL Image instance
        :returns [tuple(2)]: of (bool - result, str - error message)
        """
        error_list = []
        width, height = image.size
        if width > self.max_width:
            error_list.append(
                'Max width should be {max_width} but it was {width}'.format(
                    max_width=self.max_width, width=width
                )
            )
        if height > self.max_height:
            error_list.append(
                'Max height should be {max_height} '
                'but it was {height}'.format(
                    max_height=self.max_height, height=height
                )
            )
        if len(error_list):
            return False, '. '.join(error_list)
        else:
            return True, ''

    def get_text_pixels(self, image, palette=None):
        raise NotImplementedError(
            'get_text_pixels method has to be implemented in derived class.'
        )

    def render(self, image, palette=None):
        """
        Main TextDrawer method.
        Renders given Image instance or file_name as semi-graphics string.
        Throws: IOError, ValueError

        :param image [Image|str]: PIL Image instance
            or file_name of the file to load
        :param palette [object]: optional custom ColorPalette instance
            that will override the default
        :return [str]: string containing rendered semi-graphics
        """
        if isinstance(image, basestring):
            image = Image.open(image)  # will throw IOError if not exists
        is_valid, error_msg = self.image_is_valid(image)
        if not is_valid:
            raise ValueError(error_msg)
        palette = palette or self.palette

        text_pixels = self.get_text_pixels(image, palette)
        output_list = []
        for row in text_pixels:
            # for all text pixel rows:

            for text_pixel in row:
                # for all text pixels:
                output_list.append(text_pixel)

            output_list.append(self.TEXT_ROW_END_DIRECTIVE)
            # resets bg and adds new line at the end of each row
        return ''.join(output_list)


