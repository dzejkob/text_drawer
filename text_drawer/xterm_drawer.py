# -*- coding: utf-8 -*-

"""
Renders semi-graphics images in XTerm256 mode.
"""

import math

from text_drawer.core import (
    BaseTextDrawer, BaseColorPalette, UNICODE_CHAR_CODES
)


class CustomColorPalette(object):
    """
    Base for XTerm256 color palettes.
    """
    factor = 255 / 5.0

    def find_color(self, color):
        """
        Different implementation not using COLOR_MAP.
        """
        factor = self.factor
        return (
            int(round(color[0]/factor)),
            int(round(color[1]/factor)),
            int(round(color[2]/factor))
        )

    def render_text_pixel(self, upper_pixel, lower_pixel, prev_rendered=None):
        """
        Required interface needed by the base class.
        """
        # upper pixel:
        upper_param = u'{0}{1}{2}'.format(*self.find_color(upper_pixel))

        # lower pixel:
        lower_param = u'{0}{1}{2}'.format(*self.find_color(lower_pixel))

        # we have to make fg/bg coloring even if full block, because
        # full_block appears to not cover all the space
        coloring = (
            u'{{{0}{{[{1}'.format(lower_param, upper_param)
        )
        if upper_param == lower_param:
            # both pixels are the same color
            character = UNICODE_CHAR_CODES['full_block']
        else:
            # pixels differ
            character = UNICODE_CHAR_CODES['lower_half_block']

        rendered = u'{0}{1}'.format(coloring, character)
        if prev_rendered == rendered:
            rendered = character  # omit coloring

        return rendered


class XTerm256ColorPalette(CustomColorPalette):
    """
    XTerm256 color palette.
    """


class XTermTextDrawer(BaseTextDrawer):
    """
    Renders color semi-graphics pictures from the given image.
    """
    TEXT_ROW_END_DIRECTIVE = u'{/'
    PALETTE_CLASS = XTerm256ColorPalette

    def get_text_pixels(self, image, palette=None):
        """
        Builds TextPixel array from the image using ANSI escape codes

        :param image [Image]: PIL Image instance
        :param palette [Object]: custom ColorPalette instance
            to override default
        :returns [array(array)]: 2 dimensional array of TextPixels
        """
        width, height = image.size
        palette = palette or self.palette
        pixel_array = image.load()
        text_pixels = []

        for i in range(int(math.ceil(height / 2.0))):
            # for every two image pixel rows build one text pixel row:
            text_row = []
            rendered_pixel = None
            for x in range(width):
                # for every pixel in a row:
                upper_color = pixel_array[x, 2*i]
                try:
                    lower_color = pixel_array[x, 2*i + 1]
                except IndexError:
                    # if the image height is odd number then we will fall
                    # outside of the image in the last row
                    lower_color = (0, 0, 0)
                if not self.COMPRESS_ENABLED:
                    rendered_pixel = None
                    # reset previously rendered pixel to turn off compression
                rendered_pixel = palette.render_text_pixel(
                    upper_color, lower_color, rendered_pixel
                )
                text_row.append(rendered_pixel)
            text_pixels.append(text_row)

        return text_pixels
