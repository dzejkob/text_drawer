# -*- coding: utf-8 -*-

from distutils.core import setup


setup(
    name='text_drawer',
    version='1.0',
    packages=['text_drawer'],
    requires=['PIL']
)
